from app import db

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(200) )
    description = db.Column(db.String(200),)
    start_date = db.Column(db.DateTime)
    completed = db.Column(db.Boolean)
