from flask_wtf import Form
from wtforms import IntegerField
from wtforms import TextAreaField
from wtforms import TextField

from wtforms.validators import DataRequired

class TodoForm(Form):
    task_name = TextField('task_name', validators=[DataRequired()])
    task_description = TextField('task_description', validators=[DataRequired()])
