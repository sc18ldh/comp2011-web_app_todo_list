from flask import render_template, flash, url_for, redirect, request
from app import app
from .forms import TodoForm
from app import db, models
import datetime


@app.route('/', methods=['GET', 'POST'])
def createForm():
    form = TodoForm()



    if form.validate_on_submit():

        x = models.Todo(task=form.task_name.data, description=form.task_description.data, start_date=datetime.datetime.utcnow(), completed=0)
        db.session.add(x)
        db.session.commit()

        return redirect(url_for('createForm'))

    return render_template('create_task.html',
                           title='Create task',
                           form=form)

@app.route('/page', methods=['GET', 'POST'])
def completedTasks():


    completed_tasks = models.Todo.query.filter_by(completed=1).all()

    return render_template('completed_tasks.html', title = 'Completed Tasks', completed_tasks=completed_tasks)

@app.route('/page2', methods=['GET', 'POST'])
def incompleteTasks():


    completed_tasks = models.Todo.query.filter_by(completed=0).all()

    return render_template('incomplete_tasks.html', title = 'Completed Tasks', completed_tasks=completed_tasks)

@app.route('/complete/<id>')
def completeTask(id):

    todoItem = models.Todo.query.filter_by(id=int(id)).first()
    todoItem.completed = True
    db.session.commit()

    return redirect(url_for('incompleteTasks'))


# @app.route('/page2', methods=['GET', 'POST'])
# def incompleteTasks():
#
#     incomplete_tasks = models.Todo.query.all()
#
#     return render_template('incomplete_tasks.html', title = 'Incomplete Tasks', incompleted_tasks=incomplete_tasks)


@app.after_request
def add_header(r):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r
